import { StyleSheet } from "react-native";

export const Colors = {
  theme: "#ffbb00",
  white: "#ffffff",
  black: "#000000",
  grey: "#b5abab"
};

export const StackNavigationOptions = {
  headerStyle: {
    backgroundColor: Colors.theme
  },
  headerTintColor: Colors.white,
  headerTitleStyle: [
    {
      fontSize: 18,
      fontWeight: "bold"
    }
  ],
  headerBackTitle: null,
  gesturesEnabled: false
};

export const CommonStyles = StyleSheet.create({
  fieldValue: {
    fontSize: 16,
    color: Colors.black
  },
  fieldHeading: {
    fontSize: 16,
    color: Colors.black,
    fontWeight: "bold"
  },
  buttonText: {
    fontSize: 18,
    color: Colors.white,
    fontWeight: "bold",
    padding: 10
  },
  buttonBg: {
    backgroundColor: Colors.theme,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  inputBg: {
    borderBottomColor: Colors.grey,
    borderBottomWidth: 1
  }
});
