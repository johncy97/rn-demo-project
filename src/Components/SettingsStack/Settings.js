import React, { Component } from "react";
import { SafeAreaView, Text } from "react-native";
import LeftDrawerIcon from "../Navigation/LeftDrawerIcon";
import { StackNavigationOptions } from "../../assets/CommonStyles";

export default class Settings extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Settings",
    headerLeft: <LeftDrawerIcon navigation={navigation} />,
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
      >
        <Text>Settings screen</Text>
      </SafeAreaView>
    );
  }
}
