import React, { Component } from "react";
import {
  SafeAreaView,
  TouchableOpacity,
  View,
  Text,
  TextInput
} from "react-native";
import {
  StackNavigationOptions,
  Colors,
  CommonStyles
} from "../../assets/CommonStyles";
import RouteNames from "../Navigation/RouteNames";

export default class Login extends Component {
  static navigationOptions = () => ({
    title: "Login",
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
    this.state = {
      user_name: null,
      password: null
    };
  }

  onChangeInput(stateName, value) {
    this.setState({
      [stateName]: value
    });
  }

  onSubmitLogin() {
    this.props.navigation.navigate(RouteNames.AppStack.mainMenu);
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, padding: 20 }}>
        <View style={{ marginBottom: 10 }}>
          <Text style={[CommonStyles.fieldHeading]}>Enter User Name</Text>
          <TextInput
            underlineColorAndroid="transparent"
            blurOnSubmit={true}
            selectionColor={Colors.theme}
            style={[CommonStyles.fieldValue, CommonStyles.inputBg]}
            onChangeText={value => this.onChangeInput("user_name", value)}
            onSubmitEditing={() => this.password.focus()}
          />
        </View>
        <View style={{ marginBottom: 10 }}>
          <Text style={[CommonStyles.fieldHeading]}>Enter Password</Text>
          <TextInput
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            blurOnSubmit={true}
            selectionColor={Colors.theme}
            ref={input => {
              this.password = input;
            }}
            style={[CommonStyles.fieldValue, CommonStyles.inputBg]}
            onChangeText={value => this.onChangeInput("password", value)}
            onSubmitEditing={() => this.onSubmitLogin()}
          />
        </View>
        <TouchableOpacity
          style={[CommonStyles.buttonBg, { marginTop: 25 }]}
          onPress={() => this.onSubmitLogin()}
        >
          <Text style={[CommonStyles.buttonText]}>Login</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
