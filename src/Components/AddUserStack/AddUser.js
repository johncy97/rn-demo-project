import React, { Component } from "react";
import { SafeAreaView, Text } from "react-native";
import LeftDrawerIcon from "../Navigation/LeftDrawerIcon";
import { StackNavigationOptions } from "../../assets/CommonStyles";

export default class AddUser extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Add User",
    headerLeft: <LeftDrawerIcon navigation={navigation} />,
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
      >
        <Text>AddUser screen</Text>
      </SafeAreaView>
    );
  }
}
