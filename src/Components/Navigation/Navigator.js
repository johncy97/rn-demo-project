import React from "react";
import {
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  DrawerActions
} from "react-navigation";
import RouteNames from "./RouteNames";
import UserList from "../Home/UserList";
import Login from "../Authentication/Login";
import AddUser from "../AddUserStack/AddUser";
import Settings from "../SettingsStack/Settings";
import UserDetails from "../UserDetailDrawer/UserDetails";
import EditUser from "../UserDetailDrawer/EditUser";
import DeleteUser from "../UserDetailDrawer/DeleteUser";
import { StackNavigationOptions } from "../../assets/CommonStyles";
import MainDrawer from "./MainDrawer";
import UserDetailsDrawer from "../UserDetailDrawer/UserDetailsDrawer";
import DrawerIcon from "./LeftDrawerIcon";

const UserDetailsStack = createStackNavigator(
  {
    [RouteNames.UserDetailStack.userDetails]: {
      screen: UserDetails
    }
  },
  {
    initialRouteName: RouteNames.UserDetailStack.userDetails,
    headerMode: "float"
  }
);

const EditUserStack = createStackNavigator(
  {
    [RouteNames.EditUserStack.editUser]: {
      screen: EditUser
    }
  },
  {
    initialRouteName: RouteNames.EditUserStack.editUser,
    headerMode: "float"
  }
);

const DeleteUserStack = createStackNavigator(
  {
    [RouteNames.DeleteUserStack.deleteUser]: {
      screen: DeleteUser
    }
  },
  {
    initialRouteName: RouteNames.DeleteUserStack.deleteUser,
    headerMode: "float"
  }
);

const UserDetailMenu = createDrawerNavigator(
  {
    [RouteNames.UserDetailMenu.userDetailsStack]: {
      screen: UserDetailsStack
    },
    [RouteNames.UserDetailMenu.editUserStack]: {
      screen: EditUserStack
    },
    [RouteNames.UserDetailMenu.deleteUserStack]: {
      screen: DeleteUserStack
    }
  },
  {
    initialRouteName: RouteNames.UserDetailMenu.userDetailsStack,
    getCustomActionCreators: (route, stateKey) => {
      return {
        toggleRightDrawer: () => DrawerActions.toggleDrawer({ key: stateKey })
      };
    },
    drawerPosition: "right",
    drawerWidth: 250,
    drawerLockMode: "locked-closed",
    drawerOpenRoute: "RightDrawerOpen",
    drawerCloseRoute: "RightDrawerClose",
    drawerToggleRoute: "RightDrawerToggle",
    navigationOptions: ({ navigation }) => ({
      headerRight: <DrawerIcon navigation={navigation} />
    }),
    contentComponent: props => <UserDetailsDrawer {...props} />
  }
);

const HomeStack = createStackNavigator(
  {
    [RouteNames.HomeStack.usersList]: {
      screen: UserList
    }
  },
  {
    initialRouteName: RouteNames.HomeStack.usersList,
    headerMode: "float",
    navigationOptions: StackNavigationOptions
  }
);

const AddUserStack = createStackNavigator(
  {
    [RouteNames.AddUserStack.addUser]: {
      screen: AddUser
    }
  },
  {
    initialRouteName: RouteNames.AddUserStack.addUser,
    headerMode: "float",
    navigationOptions: StackNavigationOptions
  }
);

const SettingsStack = createStackNavigator(
  {
    [RouteNames.SettingsStack.settings]: {
      screen: Settings
    }
  },
  {
    initialRouteName: RouteNames.SettingsStack.settings,
    headerMode: "float",
    navigationOptions: StackNavigationOptions
  }
);

const AuthStack = createStackNavigator(
  {
    [RouteNames.AuthStack.login]: {
      screen: Login
    }
  },
  {
    initialRouteName: RouteNames.AuthStack.login,
    headerMode: "float",
    navigationOptions: StackNavigationOptions
  }
);

const MainMenu = createDrawerNavigator(
  {
    [RouteNames.MainMenu.homeStack]: {
      screen: HomeStack
    },
    [RouteNames.MainMenu.addUserStack]: {
      screen: AddUserStack
    },
    [RouteNames.MainMenu.settingsStack]: {
      screen: SettingsStack
    },
    [RouteNames.MainMenu.userDetailStack]: {
      screen: UserDetailMenu
    }
  },
  {
    initialRouteName: RouteNames.MainMenu.homeStack,
    drawerPosition: "left",
    drawerLockMode: "locked-closed",
    getCustomActionCreators: (route, stateKey) => {
      return {
        toggleLeftDrawer: () => DrawerActions.toggleDrawer({ key: stateKey })
      };
    },
    drawerWidth: 250,
    contentComponent: props => <MainDrawer {...props} />,
    drawerOpenRoute: "LeftDrawerOpen",
    drawerCloseRoute: "LeftDrawerClose",
    drawerToggleRoute: "LeftDrawerToggle",
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

const AppStack = createSwitchNavigator(
  {
    [RouteNames.AppStack.authStack]: {
      screen: AuthStack
    },
    [RouteNames.AppStack.mainMenu]: {
      screen: MainMenu
    }
  },
  {
    initialRouteName: RouteNames.AppStack.authStack
  }
);

const AppContainer = createAppContainer(AppStack);

export default AppContainer;
