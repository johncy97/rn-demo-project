const RouteNames = {
  HomeStack: {
    usersList: "usersList",
    userDetailMenu: "userDetailMenu"
  },
  AppStack: {
    authStack: "authStack",
    mainMenu: "mainMenu"
  },
  MainMenu: {
    homeStack: "homeStack",
    addUserStack: "addUserStack",
    settingsStack: "settingsStack",
    userDetailStack: "userDetailStack"
  },
  AddUserStack: {
    addUser: "addUser"
  },
  SettingsStack: {
    settings: "settings"
  },
  UserDetailMenu: {
    userDetailsStack: "userDetailsStack",
    editUserStack: "editUserStack",
    deleteUserStack: "deleteUserStack"
  },
  UserDetailStack: {
    userDetails: "userDetails"
  },
  EditUserStack: {
    editUser: "editUser"
  },
  DeleteUserStack: {
    deleteUser: "deleteUser"
  },
  AuthStack: {
    login: "login"
  }
};

export default RouteNames;
