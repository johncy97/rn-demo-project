import React, { Component } from "react";
import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  FlatList
} from "react-native";
import { NavigationActions } from "react-navigation";
import RouteNames from "./RouteNames";
import { Colors, CommonStyles } from "../../assets/CommonStyles";

class MenuRow extends Component {
  render() {
    const selectedColor = this.props.isSelected ? Colors.theme : Colors.black;
    return (
      <TouchableOpacity
        style={{ padding: 10, justifyContent: "center" }}
        onPress={() => {
          this.props.selectItem(this.props.itemIndex);
        }}
      >
        <Text
          style={[{ fontSize: 16, color: selectedColor, fontWeight: "bold" }]}
        >
          {this.props.menuName}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default class MainDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: RouteNames.HomeStack.usersList,
      menu: [
        {
          name: "Home",
          routeName: RouteNames.MainMenu.homeStack,
          initialRoutName: RouteNames.HomeStack.usersList
        },
        {
          name: "Add User",
          routeName: RouteNames.MainMenu.addUserStack,
          initialRoutName: RouteNames.AddUserStack.addUser
        },
        {
          name: "Settings",
          routeName: RouteNames.MainMenu.settingsStack,
          initialRoutName: RouteNames.SettingsStack.settings
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedMenu: nextProps.activeItemKey
    });
  }

  navigateTo(rowIndex) {
    let menuDetail = this.state.menu[rowIndex];
    this.props.navigation.closeDrawer();
    const navigateAction = NavigationActions.navigate({
      routeName: menuDetail.routeName,
      params: {},
      action: NavigationActions.navigate({
        routeName: menuDetail.initialRoutName
      })
    });
    this.props.navigation.dispatch(navigateAction);
  }

  renderRow(rowData, index) {
    return (
      <MenuRow
        menuName={rowData.name}
        isSelected={rowData.routeName == this.state.selectedMenu ? true : false}
        selectItem={this.navigateTo.bind(this)}
        itemIndex={index}
      />
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: Colors.grey
        }}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <FlatList
          data={this.state.menu}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => item.routeName}
          extraData={this.state}
          renderItem={({ item, index }) => this.renderRow(item, index)}
        />
        <TouchableOpacity
          style={[CommonStyles.buttonBg]}
          onPress={() => {
            this.props.navigation.closeDrawer();
            this.props.navigation.navigate(RouteNames.AppStack.authStack);
          }}
        >
          <Text style={[CommonStyles.buttonText]}>Logout</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
