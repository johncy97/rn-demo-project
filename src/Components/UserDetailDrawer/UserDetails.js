import React, { Component } from "react";
import { SafeAreaView, View, Text, Image } from "react-native";
import {
  StackNavigationOptions,
  CommonStyles
} from "../../assets/CommonStyles";
import LeftDrawerIcon from "../Navigation/LeftDrawerIcon";
import RightDrawerIcon from "./RightDrawerIcon";

export default class UserDetails extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "User Details",
    headerLeft: <LeftDrawerIcon navigation={navigation} />,
    headerRight: (
      <View
        style={{
          paddingRight: 10,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <RightDrawerIcon navigation={navigation} />
      </View>
    ),
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
    this.state = {
      user: this.props.navigation.state.params.user
    };
  }

  componentWillMount() {
    this.setState({
      user: this.props.navigation.state.params.user
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      user: nextProps.navigation.state.params.user
    });
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Image
          source={{ uri: this.state.user.avatar }}
          style={{ width: "100%", height: 250, resizeMode: "cover" }}
        />
        <Text style={[CommonStyles.fieldHeading, { paddingTop: 10 }]}>
          {this.state.user.first_name} {this.state.user.last_name}
        </Text>
      </SafeAreaView>
    );
  }
}
