import React, { Component } from "react";
import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  FlatList
} from "react-native";
import { NavigationActions } from "react-navigation";
import { Colors } from "../../assets/CommonStyles";
import RouteNames from "../Navigation/RouteNames";

class MenuRow extends Component {
  render() {
    const selectedColor = this.props.isSelected ? Colors.theme : Colors.black;
    return (
      <TouchableOpacity
        style={{ padding: 10, justifyContent: "center" }}
        onPress={() => {
          this.props.selectItem(this.props.itemIndex);
        }}
      >
        <Text
          style={[{ fontSize: 16, color: selectedColor, fontWeight: "bold" }]}
        >
          {this.props.menuName}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default class UserDetailsDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: RouteNames.UserDetailMenu.userDetailsStack,
      menu: [
        {
          name: "Edit User",
          routeName: RouteNames.UserDetailMenu.editUserStack
        },
        {
          name: "Delete User",
          routeName: RouteNames.UserDetailMenu.deleteUserStack
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedMenu: nextProps.activeItemKey
    });
  }

  navigateTo(rowIndex) {
    let menuDetail = this.state.menu[rowIndex];
    this.props.navigation.closeDrawer();
    const navigateAction = NavigationActions.navigate({
      routeName: menuDetail.routeName
    });
    this.props.navigation.dispatch(navigateAction);
  }

  renderRow(rowData, index) {
    return (
      <MenuRow
        menuName={rowData.name}
        isSelected={rowData.routeName == this.state.selectedMenu ? true : false}
        selectItem={this.navigateTo.bind(this)}
        itemIndex={index}
      />
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: Colors.grey
        }}
      />
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <FlatList
          data={this.state.menu}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => item.routeName}
          extraData={this.state}
          renderItem={({ item, index }) => this.renderRow(item, index)}
        />
      </SafeAreaView>
    );
  }
}
