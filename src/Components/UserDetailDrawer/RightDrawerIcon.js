import React, { Component } from "react";
import { TouchableOpacity, Image } from "react-native";
import { Colors } from "../../assets/CommonStyles";

export default class RightDrawerIcon extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.toggleRightDrawer()}
        style={{
          justifyContent: "center",
          alignItems: "center",
          paddingLeft: 10
        }}
      >
        <Image
          source={require("../../assets/menu.png")}
          style={{
            width: 20,
            height: 20,
            resizeMode: "cover",
            tintColor: Colors.white
          }}
        />
      </TouchableOpacity>
    );
  }
}
