import React, { Component } from "react";
import { SafeAreaView, View, Text } from "react-native";
import LeftDrawerIcon from "../Navigation/LeftDrawerIcon";
import { StackNavigationOptions } from "../../assets/CommonStyles";
import RightDrawerIcon from "./RightDrawerIcon";

export default class DeleteUser extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Delete User",
    headerLeft: <LeftDrawerIcon navigation={navigation} />,
    headerRight: (
      <View
        style={{
          paddingRight: 10,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <RightDrawerIcon navigation={navigation} />
      </View>
    ),
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
      >
        <Text>DeleteUser screen</Text>
      </SafeAreaView>
    );
  }
}
