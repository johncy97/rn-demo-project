import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList
} from "react-native";
import {
  StackNavigationOptions,
  Colors,
  CommonStyles
} from "../../assets/CommonStyles";
import { NavigationActions } from "react-navigation";
import LeftDrawerIcon from "../Navigation/LeftDrawerIcon";
import RouteNames from "../Navigation/RouteNames";

export default class UserList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "User List",
    headerLeft: <LeftDrawerIcon navigation={navigation} />,
    ...StackNavigationOptions
  });

  constructor(props) {
    super(props);
    this.state = {
      users: [],
      response: {},
      loading: false,
      pageNo: 1
    };
  }

  componentWillMount() {
    this.fetchUsers();
  }

  fetchUsers() {
    if (
      this.state.pageNo == 1 ||
      (this.state.response.total_pages &&
        this.state.response.total_pages >= this.state.pageNo)
    ) {
      this.setState(
        {
          loading: true
        },
        () => {
          fetch(`https://reqres.in/api/users?page=${this.state.pageNo}`, {
            method: "GET"
          })
            .then(res => res.json())
            .then(response => {
              this.setState({
                response: response,
                pageNo: response.page + 1,
                users: [...this.state.users, ...response.data]
              });
            })
            .catch(error => console.log("Error Occured"));
        }
      );
      this.setState({
        loading: false
      });
    }
  }

  renderEachUser(user, index) {
    return (
      <TouchableOpacity
        style={{
          flexDirection: "row",
          marginBottom: 10,
          backgroundColor: Colors.grey,
          alignItems: "center"
        }}
        onPress={() => {
          const navigate = NavigationActions.navigate({
            routeName: RouteNames.MainMenu.userDetailStack,
            action: NavigationActions.navigate({
              routeName: RouteNames.UserDetailStack.userDetails,
              params: {
                user: user
              }
            })
          });
          this.props.navigation.dispatch(navigate);
        }}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            paddingLeft: 10
          }}
        >
          <Image
            source={{ uri: user.avatar }}
            style={{ width: 50, height: 50, resizeMode: "cover" }}
          />
        </View>
        <Text style={[CommonStyles.fieldHeading, { paddingLeft: 10 }]}>
          {user.first_name} {user.last_name}
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <FlatList
          style={{ flex: 1 }}
          data={this.state.users}
          refreshing={this.state.loading}
          onRefresh={() => this.fetchUsers()}
          extraData={this.state.response}
          renderItem={({ item, index }) => this.renderEachUser(item, index)}
          onEndReached={() => this.fetchUsers()}
          onEndReachedThreshold={0}
        />
      </SafeAreaView>
    );
  }
}
