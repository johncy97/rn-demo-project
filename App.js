import React, { Component } from "react";
import { View } from "react-native";
import AppContainer from "./src/Components/Navigation/Navigator";

export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
      </View>
    );
  }
}
