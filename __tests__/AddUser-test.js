import 'react-native';
import React from 'react';
import AddUser from '../src/Components/AddUserStack/AddUser';

import renderer from 'react-test-renderer';

it('renders correctly',()=>{
    const tree = renderer.create(
        <AddUser/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
})