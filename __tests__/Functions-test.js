import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import Login from '../src/Components/Authentication/Login';

it("Functions and State testing",()=>{
    let LoginInstance = renderer.create(<Login/>).getInstance();
    
    LoginInstance.onChangeInput('user_name','User');

    expect(LoginInstance.state.user_name).toEqual('User');
    
})